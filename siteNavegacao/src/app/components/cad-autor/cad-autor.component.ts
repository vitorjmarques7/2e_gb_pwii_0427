import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';

@Component({
  selector: 'app-cad-autor',
  templateUrl: './cad-autor.component.html',
  styleUrls: ['./cad-autor.component.scss']
})
export class CadAutorComponent implements OnInit {
  aut: Autor

  constructor(
    private autoresService: AutoresService
  ) { 
    this.aut = new Autor()
  }

  ngOnInit(): void {
  }

  cadastrar (): void {
    console.log(this.aut)

    this.autoresService.cadastrar(this.aut)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
