import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEditoraComponent } from './show-editora.component';

describe('ShowEditoraComponent', () => {
  let component: ShowEditoraComponent;
  let fixture: ComponentFixture<ShowEditoraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEditoraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEditoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
