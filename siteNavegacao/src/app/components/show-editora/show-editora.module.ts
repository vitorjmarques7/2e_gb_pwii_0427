import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowEditoraRoutingModule } from './show-editora-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowEditoraRoutingModule
  ]
})
export class ShowEditoraModule { }
