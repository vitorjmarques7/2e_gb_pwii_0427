import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowEditoraComponent } from './show-editora.component';

const routes: Routes = [
  {
    path: "",
    component: ShowEditoraComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowEditoraRoutingModule { }
