import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowUsuarioRoutingModule } from './show-usuario-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowUsuarioRoutingModule
  ]
})
export class ShowUsuarioModule { }
