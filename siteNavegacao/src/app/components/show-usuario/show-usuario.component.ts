import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-show-usuario',
  templateUrl: './show-usuario.component.html',
  styleUrls: ['./show-usuario.component.scss']
})
export class ShowUsuarioComponent implements OnInit {
  usuarios: Usuario[]

  constructor(
    private usuariosService: UsuariosService
  ) { 
    this.usuarios = []
  }

  ngOnInit(): void {
    this.usuariosService.buscar().subscribe({
      next: (resposta) => {
        this.usuarios = resposta.results
      },
      error: (erro) => {
        console.error (erro)
      }
    })
  }
}
