import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowUsuarioComponent } from './show-usuario.component';

const routes: Routes = [
  {
    path: "",
    component: ShowUsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowUsuarioRoutingModule { }
