import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadUsuarioRoutingModule } from './cad-usuario-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadUsuarioRoutingModule
  ]
})
export class CadUsuarioModule { }
