import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadLivroComponent } from './cad-livro.component';

const routes: Routes = [
  {
    path: "",
    component: CadLivroComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadLivroRoutingModule { }
