import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowAutorRoutingModule } from './show-autor-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowAutorRoutingModule
  ]
})
export class ShowAutorModule { }
