import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowLivroComponent } from './show-livro.component';

describe('ShowLivroComponent', () => {
  let component: ShowLivroComponent;
  let fixture: ComponentFixture<ShowLivroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowLivroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowLivroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
