import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowLivroComponent } from './show-livro.component';

const routes: Routes = [
  {
    path: "",
    component: ShowLivroComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowLivroRoutingModule { }
