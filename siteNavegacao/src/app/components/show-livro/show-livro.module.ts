import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowLivroRoutingModule } from './show-livro-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowLivroRoutingModule
  ]
})
export class ShowLivroModule { }
