import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/models/Livro';
import { LivrosService } from 'src/app/services/livros/livros.service';

@Component({
  selector: 'app-show-livro',
  templateUrl: './show-livro.component.html',
  styleUrls: ['./show-livro.component.scss']
})
export class ShowLivroComponent implements OnInit {
  livros: Livro[]

  constructor(
    private livrosService: LivrosService
  ) { 
    this.livros = []
  }

  ngOnInit(): void {
    this.livrosService.buscar().subscribe({
      next: (resposta) => {
        this.livros = resposta.results
      },
      error: (erro) => {
        console.error (erro)
      }
    })
  }
}
