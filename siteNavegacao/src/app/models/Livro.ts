export class Livro{
    id: number = 0
    titulo: string = ""
    resumo: string = ""
    descricao: string = ""
    paginas: number = 0
    preco: number = 0
    edicao: number = 0
    idioma: string = ""
    ano_lancamento: number = 0
    id_autor: number = 0
    id_editora: number = 0
}