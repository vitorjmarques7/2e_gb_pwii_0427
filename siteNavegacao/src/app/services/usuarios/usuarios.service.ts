import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-opgbhi9yj6b.ws-us77.gitpod.io/"
  private readonly URL_R = ""
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ) { }

  cadastrar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }

  buscar(): Observable <any> {
    return this.http.get<any>(`${this.URL}usuarios`)
  }

}
