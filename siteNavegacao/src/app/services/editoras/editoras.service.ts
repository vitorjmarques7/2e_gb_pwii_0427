import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Editora } from 'src/app/models/Editora';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-opgbhi9yj6b.ws-us77.gitpod.io/"
  private readonly URL_R = ""
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ) { }

  cadastrar(editora: Editora): Observable <any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }

  buscar(): Observable <any> {
    return this.http.get<any>(`${this.URL}editoras`)
  }

}
