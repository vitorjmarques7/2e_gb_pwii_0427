import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { CadEditoraComponent } from './components/cad-editora/cad-editora.component';
import { CadLivroComponent } from './components/cad-livro/cad-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LivrosService } from './services/livros/livros.service';
import { AutoresService } from './services/autores/autores.service';
import { CadUsuarioComponent } from './components/cad-usuario/cad-usuario.component';
import { FormsModule } from '@angular/forms';
import { ShowAutorComponent } from './components/show-autor/show-autor.component';
import { ShowEditoraComponent } from './components/show-editora/show-editora.component';
import { ShowUsuarioComponent } from './components/show-usuario/show-usuario.component';
import { ShowLivroComponent } from './components/show-livro/show-livro.component';

@NgModule({
  declarations: [
    AppComponent,
    CadAutorComponent,
    CadEditoraComponent,
    CadLivroComponent,
    PageNotFoundComponent,
    CadUsuarioComponent,
    ShowAutorComponent,
    ShowEditoraComponent,
    ShowLivroComponent,
    ShowUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    LivrosService,
    AutoresService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
